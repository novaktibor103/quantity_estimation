#=
This module contains methods required for the frame duration calculation.

N: frame duration vector
T: frame time
tau: ON life time
R: read-ou time of the camera
H: minimal photon collection time for the detection
=#

module frameDuration

export PMF, F_N_constant, correction_0_frame, correction_1_frame, testing

#using Plots

# calculate the probability mass function:
function PMF(N, T, tau, R, H)
const_F_N=frameDuration.F_N_constant(T, tau, R, H)
#P_N=tau/T*(exp(T/tau)-1)^2*exp(-(R+2*H)/tau)*exp.(-(N*T/tau));
P_N=const_F_N*exp.(-(N*T/tau));
# apply the correction factors:
P_N=P_N+(N.==0)*frameDuration.correction_0_frame(T, tau, R, H)
P_N=P_N+(N.==1)*frameDuration.correction_1_frame(T, tau, R, H)
end


# the multiplication constant of the mass function
function F_N_constant(T, tau, R, H)
const_F_N=tau/T*(exp(T/tau)-1)^2*exp(-(R+2*H)/tau)
end


# coorection factor for the 0 frame case
function correction_0_frame(T, tau, R, H)
P_0_corr=1-tau/T*exp(-(2*H+R-T)/tau)*(exp(T/tau)-2)-(T-R-H+tau)/T*exp(-H/tau);
end


# coorection factor for the 1 frame case
function correction_1_frame(T, tau, R, H)
P_1_corr=(tau+T-R-H)/T*exp(-H/tau)-tau/T*exp(-(R+2*H-T)/tau);
end


# show the histogram ofthe probility mass function
function PMF_histogram(N, T, tau, R, H)
# TODo
distribution=PMF(N, T, tau, R, H)

#histogram(distribution)

end


# for checking the correctness of the calculation
function testing(N_max, T, tau, R, H)

N=0:N_max
# get the required results:
P_N=frameDuration.PMF(N, T, tau, R, H)
P_0_corr=correction_0_frame(T, tau, R, H)
P_1_corr=correction_1_frame(T, tau, R, H)
# formulata for the 0 frame probability from some previous calculation/derivation
P_0=1-(tau+T-R-H)/T*exp(-H/tau)+tau/T*exp(-(R+2*H)/tau)

# checking the summation of the geometric series
#const_F_N=F_N_constant(T, tau, R, H)
#sum1_1=const_F_N*1/(exp(T/tau)-1)+P_1_corr
#sum1_2=sum(P_N[2:end])
#println(const_F_N)
#println("sum from 1: " * string(sum1_1) * "\\" * string(sum1_2))

# juxtapose the current and the old formula for the single frame case
println("P0: " * string(P_N[1]) * "\\" * string(P_0))
# sum of the probability mass function
println("sum: " * string(sum(P_N)))

end

end

