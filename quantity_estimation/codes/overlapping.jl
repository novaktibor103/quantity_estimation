include("frameDuration.jl")

# define the required constants
T=200;
H=5;
R=2;
tau=1000;

#t_b=2*T-2*H;
t_b=2*T;

ro_0=1/30000;



# get the required constants:
const_F_N=frameDuration.F_N_constant(T, tau, R, H);
F_1_corr=frameDuration.correction_1_frame(T, tau, R, H);

# numerator of the probability:
a=exp(-ro_0*tau)*exp(-ro_0*t_b)*(const_F_N*1/(exp(T*ro_0+T/tau)-1)+exp(-T*ro_0)*F_1_corr);

# denominator of the probability:
b=const_F_N*1/(exp(T/tau)-1)+F_1_corr;

# probability of being no overlap:
P_0=a/b;


println(P_0)
