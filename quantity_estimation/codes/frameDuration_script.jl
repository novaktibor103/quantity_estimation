#=
This cripts runs the frame duration calculations and shows the chosen results.
=#

include("frameDuration.jl")

# define the required constants
T=30;
tau=30;
R=2;
H=5;
N_max=1000;
N_min=0;

N=N_min:N_max;

# print the chosen results:

# correction factors:
#println(frameDuration.correction_1_frame(T, tau, R, H))
#println(frameDuration.correction_0_frame(T, tau, R, H))

# sum of the probability mass function:
#println(sum(frameDuration.PMF(N, T, tau, R, H)))

# TODo
#frameDuration.PMF_histogram(N, T, tau, R, H)


# call the method dedicated for testing
frameDuration.testing(N_max, T, tau, R, H);

