
\section{Effects of kernel functions: bias and induced variance}

In order to draw conclusions about the spatial distribution of a quantity (let it be the local intensity, the polarization degree, etc...) from the pointillistic localization data, one has to perform an averaging (or summation) using some kernel function. By using a kernel function as "smoothing" on the pointillistic data (to obtain a smoothed function), it is possible to make assumptions about the measured quantity at spatial points where no localization was recorded. Moreover, applying the rectangular kernel and evaluation it at predefined, equidistant points, one arrives at the commonly used pixelization. However, the smoothed function inherits the uncertainties of the measurement (of the single molecule information, in other words). Furthermore, using finite width kernel function has a disadvantage too, namely the bias, that is the expectation value of the smoothed function will deviate from the "real" value to be measured. In order to perform a measurement and evaluate it in a meaningful way, it is important to estimate expected values of the aforementioned errors and uncertainties.

I will derive formulas showing how the bias and the population variance (expectation value of the variance of a single blinking event, no averaging yet) depends on the chosen smoothing function. I will consider the case of using kernel functions with fixed widths (whose widths does not depend on the localization density or on some quality of the data point itself (e.g. on the localization precision)). The effects of the finite localization precision will also be taken into account, albeit only with a predefined Gaussian kernel for every localization (so neither the stochastical precision variation of the individual localizations nor its spatial dependence as in case of inhomogenous excitation will be considered). I will derive the exact formulas for smoothing a sinusodially varying quantity with rectangular and Gaussian kernels. 

\subsection{ General statements }
Let $q$ donate spatial function of the "real" value of the quantity under investigation, $P$ the localization precision function and $K$ the kernel function. For a given point-like emitter, the $P$ describes the density distribution of its localized positions around the true position. Supposing the $P$ is the same for every emitter's every blinking event, a simple convolution ($ q\circledast P $) describes the expectation value distribution of the quantity. The kernel function's effect is also described by convolution. Exploiting the associvity property of the convolution, the total smoothing caused by the finite localization precision and by the kernel can be expressed with a single function:
\begin{equation}
S=P\circledast K
\end{equation}
The quantity's expectation value distribution, its bias and the expected variance distribution can be expressed using the smoothing function (analogous to the weighted mean and weighted variance):
\begin{equation}
s=q\circledast S
\end{equation}

\begin{equation}
bias=s-q
\end{equation}

\begin{equation}
Var(q)=(q-s)^2\circledast S = (q)^2\circledast S - s^2
\end{equation}

I note that decomposing the $q$ into orthogonal function basis (e.g. Fourier series) is not fully useful for further deductions. Although the expectation value and the bias can be calculated using the sum of the individual components, but the variance can't be. For this reason I only investigate the case of a simple sinusoidal variation.

\subsection{One dimensional sinusoidal}

Sinusoidal variation (with normalized amplitude):
\begin{equation}
 q=c+\cos(k_0 x)
\end{equation}
The constant $c$ term is allowed as its covariance with the other finite frequency term is zero. The covariance would not be zero if other (non-zero frequency) components were allowed.
\begin{equation}
 bias=q\circledast S-q=(\cos(k_0 t)\circledast S)(x) - \cos(k_0 x) = const1 \cos(k_0 x) - \cos(k_0 x)
\end{equation}
\begin{equation}
 const1=\sqrt{2\pi} \mathcal{F}\{S\}(k_0)
\end{equation}
I note that the $\sqrt{2\pi}$ term appears beacuse of the chosen convention of the Fourier tranformation, see the Appendix.

\begin{equation}
  \begin{aligned}
 Var(q) = (q)^2\circledast S - s^2 = 
 ((c+\cos(k_0 t))^2 \circledast S)(x) - (c + const1 \cos(k_0 x))^2 = \\
 (\cos(k_0 t)^2 \circledast S)(x) - const1^2 \cos(k_0 x)^2 = \\
 ((\frac{1+\cos(2k_0 t))}{2} \circledast S)(x) - const1^2 \cos(k_0 x)^2 = \\
 ((\frac{1+const2 \cos(2k_0 x))- 2 const1^2 \cos(k_0 x)^2}{2}
 \end{aligned}
\end{equation}
\begin{equation}
 const2=\sqrt{2\pi} \mathcal{F}\{S\}(2 k_0)
\end{equation}
%
For the derivation of the equations above, normalized smoothing function was used.
I note that the conventional pixelization does not use normalized kernel function. I chose normalized kernel functions because the effects of different kernel widths are comparable more straightforwardly as no width dependent constant appears. Moreover, for averaging (instead of summation) we will arrive to the same results whether we use normalized or not normalized kernels.

In order to calculate the values of the $const1$ and $const2$ constants it is neccessary to calculate the Fourier transforms of the localization precision function and of the kernel function.\newline
Fourier transformation of the localization precision function in one dimension:
\begin{equation}
 \mathcal{F}\{ \frac{1}{\sqrt{2\pi} \sigma} e^{-\frac{x^2}{2 \sigma^2}} \} =
 \frac{1}{\sqrt{2\pi}} e^{-\frac{k^2 \sigma^2}{2}}
\end{equation}
Fourier transformation of the rectangular and the Gaussian kernels in one dimension:
\begin{equation}
 \mathcal{F}\{ \frac{1}{w} rect(\frac{x}{w}) \} = \frac{1}{\sqrt{2\pi}} sinc(\frac{kw}{2\pi})
\end{equation}
\begin{equation}
 \mathcal{F}\{ \frac{\sqrt{8 \ln{2}}}{\sqrt{2\pi} w} e^{-\frac{x^2 8 \ln{2}}{2 w^2}} \} =
 \frac{1}{\sqrt{2\pi}} e^{-\frac{k^2 w^2}{16 \ln{2}}}
\end{equation}
The $w$ is the width parameter of the rectangular and of the Gaussian kernels. It is now chosen as the FWHM of the two kernels in hope that the two kernels' effects will be comparable using the same $w$ value.\newline
The Fourier transform of the smoothing function can now be easly expressed:
\begin{equation}
 \mathcal{F}\{ S \} = \mathcal{F}\{ P\circledast K \} = \sqrt{2\pi} \mathcal{F}\{ P \} \mathcal{F}\{ K \}
\end{equation}
The constants for a rectangular kernel:
\begin{equation}
 const1 =  \frac{1}{\sqrt{2\pi}} e^{-\frac{k_0^2 \sigma^2}{2}}
sinc(\frac{k_0 w}{2\pi})
\end{equation}
\begin{equation}
 const2 =  \frac{1}{\sqrt{2\pi}} e^{-\frac{4 k_0^2 \sigma^2}{2}}
sinc(\frac{2k_0 w}{2\pi})
\end{equation}
The constants for a Gaussian kernel:
\begin{equation}
 const1 =  \frac{1}{\sqrt{2\pi}} e^{-\frac{k_0^2 \sigma^2}{2}}
e^{-\frac{4k_0^2 w^2}{16 \ln{2}}}
\end{equation}
\begin{equation}
 const2 =  \frac{1}{\sqrt{2\pi}} e^{-\frac{4 k_0^2 \sigma^2}{2}}
e^{-\frac{4k_0^2 w^2}{16 \ln{2}}}
\end{equation}

\subsection{Two dimensional sinusoidal}
TODO

\subsection{Appendix}

Normalized kernel functions were used:
\begin{equation}
 \int_{-\infty}^{\infty} {S} \mathrm{d}r = \int_{-\infty}^{\infty} {P} \mathrm{d}r =\int_{-\infty}^{\infty} {K} \mathrm{d}r = 1
\end{equation}

The following convention was used for the Fourier transformation and for its inverse:
\begin{equation}
\hat{f}(k)=\mathcal{F}\{f(r)\}(k)=\frac{1}{\sqrt{2\pi}^n}\int f(r) e^{-ikr} \mathrm{d}r
\end{equation}
\begin{equation}
f(r)=\mathcal{F}^{-1}\{\hat{f}(k)\}(k)=\frac{1}{\sqrt{2\pi}^n}\int \hat{f}(k) e^{ikr} \mathrm{d}k
\end{equation}
, where $n$ is the number of spatial dimensions of the pointillistic data.\newline
Using this convention, the convolution can be expressed with the Fourier transformation the following way:
\begin{equation}
f \circledast g = \mathcal{F}^{-1} \{ \sqrt{2\pi}^n \mathcal{F} \{ f \}  \mathcal{F} \{ g \}  \}
\end{equation}

Convolution of the smoothing function with a one dimensional sinusoid:
\begin{equation}
\begin{aligned}
 S \circledast cos(k_0 r' + \phi)
 = \mathcal{F}^{-1} \{ \sqrt{2 \pi} \mathcal{F}\{S\} \mathcal{F} \{ cos(k_0 r + \phi)\}  \} \\
 = \frac{1}{\sqrt{2 \pi}} \int_{-\infty}^{\infty} \sqrt{2 \pi} \mathcal{F}\{S\}(k) (c_1 \delta \{k-k_0\} + c_1 \delta \{k+k_0\}) e^{i k r} \mathrm{d} k \\
 = \sqrt{2 \pi} \frac{1}{\sqrt{2 \pi}} ( \mathcal{F}\{S\}(k_0) c_1 e^{i k_0 r} + \mathcal{F}\{S\}(-k_0) c_1 e^{- i k_0 r})
 \end{aligned}
\end{equation}
Since we chose the kernel function to be real and even, the smoothing function's Fourier transform is also real and even:
\begin{equation}
 \mathcal{F}\{S\}(k_0)=\mathcal{F}\{S\}(-k_0)
\end{equation}
, this allows us to express the convolution in a more concise form:
\begin{equation}
\begin{aligned}
\sqrt{2 \pi} \mathcal{F}\{S\}(k_0) \frac{1}{\sqrt{2 \pi}} ( c_1 e^{i k_0 r} + c_1 e^{- i k_0 r}) = \\
\sqrt{2 \pi} \mathcal{F}\{S\}(k_0) \mathcal{F}^{-1} \{ \mathcal{F} \{ cos(k_0 r' + \phi) \} \} = \\
\sqrt{2 \pi} \mathcal{F}\{S\}(k_0) cos(k_0 r + \phi)
 \end{aligned}
\end{equation}
check later: $r',\ t$
